import { NextApiRequest, NextApiResponse } from "next";

/** Function shorten the provided long link */
const shortenUrl = async (req: NextApiRequest, res: NextApiResponse) => {
  try {
    const { link } = JSON.parse(req.body);

    const postData = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
      },
      body: JSON.stringify({ link }),
    };

    // call the API to shorten the link
    const apiResponse = await fetch(`${process.env.URL_SHORTENER_API}/api/shorten`, postData); x``
    if (apiResponse.status === 201) {
      res.status(apiResponse.status).json(await apiResponse.json());
    } else {
      throw ({ message: await errorHandler(apiResponse), status: apiResponse.status });
    }
  } catch (error: any) {
    res.status(error.status).send(error);
  }
};

/** Handle error to give a meaningful message */
const errorHandler = async (error: any) => {
  let message = 'Something went wrong, please try again.';
  switch (error.statusCode) {
    case 401:
      message = error.message ? error.message : 'You are not authorized to perform this action.';
      break;

    default:
      // do nothing
      break;
  }
  return message;
}

export default shortenUrl;