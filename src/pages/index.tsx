import HomePage from '@/components/Home/Home';
import type { NextPage } from 'next'

const Home: NextPage = () => {

  return (
    <>
      <HomePage />
    </>
  )
}

export default Home
