export const Header = () => {

  return (
    <>
      <header>
        <nav className='container flex items-center py-4 mt-4 sm:mt-12'>
          <div className='py-1'> <h1>Logo</h1> </div>

          <ul className='hidden sm:flex flex-1 justify-end items-center gap-8 text-cyan-300 uppercase text-sm'>
            <li className='cursor-pointer'>Plans</li>
            <li className='cursor-pointer'>Api Docs</li>

            <button type='button' className='bg-sky-500/75 text-white rounded-md px-7 py-3'>Login</button>
            <button type='button' className='bg-indigo-500 text-white rounded-md px-7 py-3'>Sign up</button>
          </ul>
          <div className='flex sm:hidden flex-1 justify-end'>
            <span className="material-symbols-outlined">menu</span>
          </div>
        </nav>
      </header>
    </>
  )
}
