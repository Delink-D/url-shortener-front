import type { NextPage } from 'next'
import { useState } from 'react';
import { useForm } from 'react-hook-form';
import { Footer } from '../Navigation/Footer.component';
import { Header } from '../Navigation/Header.component';
import { Toast, toastList, IToast } from '../Toast/Toast';

interface IFormInputs {
  longLink: string
}

const HomePage: NextPage = () => {
  const { register, handleSubmit, formState: { errors } } = useForm<IFormInputs>({
    mode: 'onTouched',
    defaultValues: {
      longLink: 'https://'
    }
  });

  const [shortLink, setShortLink] = useState(null);
  const [link, setLink] = useState('');
  const [toastsToShow, setToastsToShow] = useState<IToast[]>([]);

  /* Handle on submit function*/
  const onSubmit = async (e: IFormInputs) => {
    const longUrl = e.longLink;

    // check if request link is our domain
    const { hostname } = new URL(longUrl);
    if (hostname === process.env.NEXT_PUBLIC_SHORT_DOMAIN_HOSTNAME) {
      showToast({ type: 'info', message: 'The URL seems to be a dot link.', duration: 6000 });
      return;
    }

    // call api to shorten link
    const response = await fetch('/api/url', { method: 'POST', body: JSON.stringify({ link: longUrl }) });
    const shortedLink = await response.json();

    if (response.status === 201) {
      // success shortening link
      setShortLink(shortedLink.data.shortLink);
      setLink(shortedLink.data.shortLink);
      showToast({ type: 'success', message: shortedLink.message, duration: 6000 });
    } else {
      showToast({ type: 'error', message: shortedLink.message, duration: 6000 });
    }
  }

  /** Function handle copy short url tto clipboard */
  const copyUrl = async () => {
    if (!shortLink) { return; }
    navigator.clipboard.writeText(shortLink);
    showToast({ type: 'info', message: 'URL copied to clipboard.', duration: 6000 });
  }

  /** Handle show toast request */
  const showToast = async ({ type, message, duration }: { type: string, message: string, duration: number }) => {
    const findToast = toastList.find((toast: IToast) => toast.title.toLowerCase() === type.toLowerCase());
    const toast: IToast = { ...(findToast ? findToast : toastList[toastList.length - 1]) };

    toast.serial = Math.floor((Math.random() * 100) + 1);
    toast.message = message;
    setToastsToShow([...toastsToShow, toast]);
  }

  return (
    <div className='font-Poppins'>

      <Header />

      <section className='container-fluid dot-container-fluid'>

      </section>

      <section className='bg_main'>
        <div className='container dot_main_input'>
          <div className='row'>
            <div className='col-12 mt-4 py-2 mb-2'>
              <form onSubmit={handleSubmit(onSubmit)}>
                <div className='input-group rounded-md'>
                  <input
                    {...register('longLink',
                      {
                        required: {
                          value: true,
                          message: 'The URL field is required.'
                        },
                        pattern: {
                          value: /((https?|ftp):\/\/|www\.)[^\s/$.?#].[^\s]*/,
                          message: 'Please provide a valid URL.'
                        }
                      }
                    )}
                    value={link}
                    type="text"
                    id="longLink"
                    className={
                      `block w-full pl-7 pr-12 sm:text-sm rounded-md bg-white border shadow-sm border-slate-300 placeholder-slate-400 focus:outline-none focus:border-sky-500/75 focus:ring-sky-500/75 focus:ring-2
                      ${errors.longLink && 'border focus:border-red-500 focus:ring-red-500 focus:ring-2'}`
                    }
                    onChange={(e) => { setLink(e.target.value); if (shortLink) { setShortLink(null) } }}
                    placeholder='Long link (https://this-is-my-long-link.com)' />

                  {(shortLink !== null && shortLink !== undefined) ?
                    <button
                      type='button'
                      className='btn_dot_shorten bg-sky-500/75 text-white rounded-md px-7 py-3' onClick={copyUrl}>Copy</button>
                    :
                    <button
                      type='submit'
                      className='btn_dot_shorten bg-sky-500/75 text-white rounded-md px-7 py-3'>Shorten</button>
                  }
                </div>
                {errors.longLink &&
                  <p className='text-sm text-red-500 pt-2'>{errors.longLink?.message}</p>
                }
              </form>
            </div>
          </div>
        </div>
      </section>

      {<Toast toastsToShow={toastsToShow} duration={6000}></Toast>}

      <Footer />
    </div>
  )
}

export default HomePage
