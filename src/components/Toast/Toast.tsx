import PropTypes from 'prop-types';
import { useEffect, useState } from 'react';
import styles from '@/styles/Toast.module.scss';

export interface IToast {
  id: number,
  serial: number,
  title: string,
  message: string,
  txtColor: string,
  bgColor: string,
  svgStyle: string,
  svgPath: string
}

// list of available toasts
export const toastList: IToast[] = [
  {
    id: 1,
    serial: 0,
    title: 'Success',
    message: '',
    txtColor: 'text-green-500',
    bgColor: 'bg-green-500 border-green-700',
    svgStyle: 'bi-check-circle',
    svgPath: 'M10.97 4.97a.235.235 0 0 0-.02.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-1.071-1.05z',
  },
  {
    id: 2,
    serial: 0,
    title: 'Info',
    message: '',
    txtColor: 'text-blue-500',
    bgColor: 'bg-blue-400 border-blue-700',
    svgStyle: 'bi-info-circle',
    svgPath: 'm8.93 6.588-2.29.287-.082.38.45.083c.294.07.352.176.288.469l-.738 3.468c-.194.897.105 1.319.808 1.319.545 0 1.178-.252 1.465-.598l.088-.416c-.2.176-.492.246-.686.246-.275 0-.375-.193-.304-.533L8.93 6.588zM9 4.5a1 1 0 1 1-2 0 1 1 0 0 1 2 0z',
  },
  {
    id: 3,
    serial: 0,
    title: 'Error',
    message: '',
    txtColor: 'text-red-500',
    bgColor: 'bg-red-500 border-red-700',
    svgStyle: 'bi-x-circle',
    svgPath: 'M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z',
  },
  {
    id: 4,
    serial: 0,
    title: 'Warning',
    message: '',
    txtColor: 'text-orange-500',
    bgColor: 'bg-orange-400 border-orange-700',
    svgStyle: 'bi-exclamation-circle',
    svgPath: 'M7.002 11a1 1 0 1 1 2 0 1 1 0 0 1-2 0zM7.1 4.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 4.995z',
  },
]

export const Toast = (props: any): JSX.Element => {
  const { toastsToShow, duration } = props;
  const [showList, setShowList] = useState([]);

  useEffect(() => {
    setShowList(toastsToShow); // set toast list
  }, [showList, toastsToShow]);

  useEffect(() => {
    const interval = setInterval(() => {
      if (toastsToShow.length && showList.length) {
        deleteToast(toastsToShow[0].id);
      }
    }, duration);

    return () => {
      clearInterval(interval);
    }
  });

  /** Handle delete toast functionality */
  const deleteToast = async (serial: number) => {
    const index = showList.findIndex((toast: IToast) => toast.serial === serial);
    showList.splice(index, 1);

    const toastListItem = toastsToShow.findIndex((toast: IToast) => toast.serial === serial);
    toastsToShow.splice(toastListItem, 1);

    setShowList([...showList]);
  }

  return (
    <div className='toast-container absolute right-0 top-24 m-5'>
      {
        showList.map((toast: any, i: number) =>

          <div key={i} className={`${styles.toast} flex items-center border-l-4 py-2 px-3 shadow-md mb-2 ${toast.bgColor}`}>
            <button className={styles.btn_close_toast} onClick={() => deleteToast(toast.serial)}>X</button>

            <div className={`${toast.txtColor} rounded-full bg-white mr-3`}>
              <svg xmlns="http://www.w3.org/2000/svg" width="1.8em" height="1.8em" fill="currentColor" className={`bi ${toast.svgStyle}`} viewBox="0 0 16 16">
                <path d='M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z' />
                <path d={`${toast.svgPath}`} />
              </svg>
            </div>

            <div>
              <h2 className='text-white'>{toast.title}</h2>
              <p className='text-white max-w-xs'>{toast.message}</p>
            </div>
          </div>
        )
      }
    </div>
  )
}

// default props
Toast.defaultProps = {
  duration: 6000
}

// props data types
Toast.protoType = {
  message: PropTypes.string.isRequired,
  toastType: PropTypes.string.isRequired
}
